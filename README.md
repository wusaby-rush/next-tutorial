> for older versions see `befor_13` branch

# server/client components dance in nextjs

**Server side**

0. by default all components are RSC
1. can't use react hooks like `useState`
2. all it's logic like fetching run on server
3. can use async/await
4. can use RCC

**Client side**

1. start file with `'use client';` to run it in client
2. cant use RSC directly, when component start with `'use client'`, all of its components will be RCC even if they does not start with `'use client'`, this enable us to make all app (or route) to SPA by make entrypoint RCC.
3. to use client component from npm, you should encapsulate it inside `'use client';` component, coz they by default doesnt start with `'use client'` for example `<Todo />` will be `<TodoProvider />`

**mental model**
Next.js try to push RCC to leafs of your app so U can import RCC in RSC to use it, but to use RSC in RCC (for example you need make root layout interactive for darkmode, without convert all page components to RCC) you should compose it in this pattern: 

```tsx
'use server'

export defualt function RSC() {

  return (
    <ReactClientComponent>
      <ReactServerComponent />
    </ReactClientComponent>
  )
}
```

we can also pass through props from RSC to RCC but prop value should be serializable (if have function it is not)


# structure

```sh
  - app/
    - not-found.tsx # for a 404 error, it display inside layout page
    - layout.tsx # accept ({ children }) prop and implemented on all it sub directories
    - loading.tsx # by default it is RSC, but also can be RCC, see explain down
    - error.tsx # should use 'use client', see explain down
    - page.tsx # /index page
    - _private_directory/ # any directory start with _ is out of routing
    - components/ # accessed by @/app/components, if doesnt have page inside it is out of routing
      - Meta.tsx # default meta tags
      - Nav.tsx # navigation
    - (dashboard)
      - sales/ # route to /sales/ not /dashboard/sales
        - page.tsx
      - customers/ # route to /customers/ not /dashboard/customers
        - page.tsx
      - layout.tsx # layout for all (dashboard) pages
    - api/
      - hello/
        - route.ts # /api/hello
    - about/
      - team/
        - page.tsx # /about/team page
        - loader.tsx # if visit /about/team take time, show loader until finish
      - [id]/ # dynamic route 
        - page.tsx # /about/:id page accept ({ params, searchParams })
      - [...categorise]/ # catch all subroutes
        - page.tsx # /about/foo, /about/foo/bar, ... pages
      - [[...categories]] # catch all-sement
        - page # /about, about/foo, /about/foo/bar
      - page.tsx # /about page
      - layout.tsx
      - loader.tsx
    - (admin)/ # grouped path doesnt show in path
      - layout.tsx
      - users/page.tsx
      - products/page.tsx
```

**NBs**

0. we can access any component by `@/app/`
1. if any route doesnt have `error` or `loading` or `layout` or `not-found`, it fall to nearest one

## Pages states

1. error page display if any component in page tree throw error
2. loading page display if only the page component take some time to show, it doesnt releated to components inside page, for components loader see `Suspense`. if there is `loading` page, nextjs will wrap page component inside `<Suspense fallback={/* loading page*/}>`, so `loading` page will show if page component is promised copmponent (promised clent/server component), or it have unhandled promised components with `Suspense`, if those promised components have thier Suspense handler and page component is not promised loader will not show 

## Metadata

change metadata from `layouts`s and `pages`s

```ts
// either Static metadata
export const metadata = {
  title: "...",
};

// or Dynamic metadata
export async function generateMetadata({ params, searchParams }) {
  return {
    title: "...",
  };
}
```


## next modules

### next/head

```tsx
impoer Link from "next/link";

export default function Page() {
  // default rplace is false
  // default prefetch is true
  return <Link href="/dashboard" replace prefetch={false}>Dashboard</Link>;
}
```

### next/navigation

>router module used with RCCs to navigate manually

```tsx
'use client'

import { useRouter, usePathname, useParams, useSearchParams } from 'next/navigation'

export default function SearchNote() {
  const router = useRouter()
  const pathname = usePathname()

  const params = useParams()
  // Route -> /shop/[tag]/[item]
  // URL -> /shop/shoes/nike-air-max-97
  // `params` -> { tag: 'shoes', item: 'nike-air-max-97' }
  console.log(params)

  const searchParams = useSearchParams()
  const search = searchParams.get('search')

  return (
    <>
      <h1> Search Note Page </h1>
      <p>Current pathname: {pathname}</p>
      {/* URL -> `/dashboard?search=my-project` */}
      {/* `search` -> 'my-project' */}
      return <>Search: {search}</>
      <button type="button" onClick={() => router.push('/notes')}></button>
    </>
  )
}
```

### next/font

```tsx
import { Poppins } from "next/font/google";

const poppins = Poppins({
  weights: [400, 700],
  styles: ["normal", "italic"],
  subsets: ["latin", "latin-ext"],
});

export default function Page() {
  return <h1 className={{ poppins.className }}>hello world</h1>;
}
```

### next/image

>for remote image sources nextjs doesnt allow use it directly for security reasons, so we should add remote sources to nextjs config like:
```js
const nextConfig = { images: { RemotePatterns: [
  {
    protocol: 'https',
    hostname: 'www.hackingwithswift.com'
  }
] } }
```

```tsx
import Image from "next/image";
import logo from '../path/to/image'

export default function Page() {
  return (
    <Image
      src={logo}
      // or src="/image.png"
      alt="image"
      width={500}
      height={500}
      layout="responsive"
      objectFit="cover"
      objectPosition="center"
      priority={true} // mean this image should downloaded whatever
      quality={100}
    />
  );
}
```

## API

```ts
export async function GET(req: Request, context: { params: any }) {
  // get request headers
  const { searchParams } = new URL(req.url)
  const name = searchParams.get('name')

  // get dynamic params easly
  const id = context.params.post
  console.log(id)

  return new Response(name)
}

export async function POST(req: Request) {
  // get requests body
  const body = await req.json()

  return NextResponse.json(p) // from next/server
}
```


# static and server modes

by default all pages and components are RSCs, main idea of react is use server components for every thing (coz it's output is pure html), and use client components only in leafs to ship less js

but we can mark a page component as client component to make all of it's route as SPA, or even mark main layout and page as client component to make hole project as SPA

on other hand, RSC pages (routes) work in two modes, static and server mode

## static mode

need to enable from next config, in this mode every RSC page (route) renderd in html file. all project can hosted without server, but not all nextjs server feauterd available

## server mode

default mode and need server, in this mode every RSC page (route) render depending on server. by default every route renderd at build (like static mode) we call this cache page, after server run we can revalidate cache (to render page again and cache it) manually or based on time

cached pages doesnt render with every request, but we can make routes rnder with every request. nextjs can detect if he should precache page (render at build time), or no depend on code you write and also support mark this manually

1. as saied by default all pages (routes) are precached, and will not re-render again
2. `export const revalidate = 600` in page (route) will make it rendered in request and cached for 600s, it will not re-renderd on server until time pass so it's content will still as it 
3. pages which any of componenets use `headers()` or `cookies()` (request functions) rendered with every request
4. routes with pure `fetch()` are pre-cached, unless page is dynamic route and we dont use `getStaticProps`, here more dtails on behaviour with `fetch`
  * `const { data } = await fetch(url, { cache: 'force-cache' })`: `force-cache` is default so dont need to add it, pre-cached page
  * `const { data } = await fetch(url, { next: { revalifate: 60 }})`: work as mentioned at point '2'
  * `const { data } = await fetch(url, { cache: 'no-store' })`: route will render with every request


# Server action

>for 13.4 version and before add `expermintal: { serverActions: true }` in next config file

server actions doesnt return feedback, so revalidate iny path you want it update it's view, the revalidated route will reload and show new data if there.

to avoid reloading use RCC with API in view you dont want to updated, server action more for actions that doesnt need update like analytcs for example

**in server components**

```tsx
import { revalidatePath } from 'next/cache'
const todos: string[] = ["learn react"]

export default Home() {
  async function addTodo(data: FormData) {
    "use server";
    const todo = data.get("todo") as string
    todos.push(todo)
    // to make server refresh and show new data
    revalidatePath("/")
  }

  return (
    <>
      <ul>
        {todos.map((todo, index) => 
          (<li key={index}>{todo}</li>)
        )}
      </ul>
      <form action={todo}>
        <input type="text" name="todo" />
        <button type="submit"> add todo </button>
      </form>
    </>
  )
}
```

improve add button to use status like:

```tsx
"use client";
import { experimental_useFormStatus as useFormStatus } from "react-dom";

export default function AddButton() {
  const { pending } = useFormStatus()

  return <button disabled={pending} type="submit"> add todo </button> 
}
```

**in client**

You can invoke Server Actions using the following methods:

- Using action: React's action prop allows invoking a Server Action on a <form> element.
- Using formAction: React's formAction prop allows handling <button>, <input type="submit">, and <input type="image"> elements in a <form>.
- Custom Invocation with startTransition: Invoke Server Actions without using action or formAction by using startTransition. This method disables Progressive Enhancement.


```tsx
// function call file
'use server'
import { revalidatePath } from 'next/cache'

export async function myAction(id) {
  // ...
  revalidatePath('/test')
}

// client component
import { myAction } from './actions';

export default Home() {

  return (
    <>
      <form action={myAction}>
        <input type="text" name="todo" />
        <button type="submit"> add todo </button>
      </form>
    </>
  )
}
```

# Middlwares
https://youtu.be/cj1trlsQ0is?si=NHAUBgaK2YwjkM9e

```js
// middlware.js
export default function middleware(request => {
  return NextResponse.redirect(new URL('/', request.url))
})

export const config = {
  matcher: ['/about/:path']
}
```

# NextAuth
https://youtu.be/1SjqRn_Ira4?si=3JP265FSE-zFX9rH
https://youtu.be/eoPpSOY2pjQ?si=VPQwPMjpXlHDfUPz

# MDX Content

# Database

> see `wusaby-rush` prisma repo
